# dropshipping-detox

Oculta todos los productos de "importación" de MercadoLibre México y Amazon México.

### Construir
Para construir el addon basta con hacer utilizar el comando:
```
web-ext build
```

### Instalar versión local
Para instalar la versión construida en local puedes:
1. Ir a `about:debugging`
2. "Este firefox"
3. Dar click en "Cargar complemento temporal.."
4. Seleccionar el .zip dentro de la carpeta `web-ext-artifacts`

Esto solo lo instala de manera temporal. Cuando reinicies Firefox, se borrará.

### Instalar versión publicada
Para instalar la versión publicada mas reciente:
1. Ir al sitio [dropshipping-detox](https://addons.mozilla.org/es/firefox/addon/dropshipping-detox/)
2. Dar click en "Agregar a Firefox"
Listo :).

### Imágenes
Mercadolibre **con** productos importados:
![MercadoLibre antes](repo-imgs/ml-before.png)
Mercadolibre **sin** productos importados:
![MercadoLibre después](repo-imgs/ml-after.png)
Amazon **con** productos importados:
![Amazon antes](repo-imgs/amazon-before.png)
Amazon **sin** productos importados:
![MercadoLibre después](repo-imgs/amazon-after.png)

