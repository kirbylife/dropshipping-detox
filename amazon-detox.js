setTimeout(() => {
    let items = document.getElementsByClassName("s-result-item");
    for(let n = 0; n < items.length; n++) {
        let item = items[n];
        let isInternational = item.innerHTML.includes("01siZb3GpxL");
        if(isInternational) {
            item.style.display = "none";
        }
    }
}, 500);
