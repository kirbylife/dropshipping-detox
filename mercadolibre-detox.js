setTimeout(() => {
    let items = document.getElementsByClassName("ui-search-layout__item");
    for(let n = 0; n < items.length; n++) {
        let item = items[n];
        let isInternational = item.innerHTML.includes("international");
        if(isInternational) {
            item.style.display = "none";
        }
    }
}, 500);
